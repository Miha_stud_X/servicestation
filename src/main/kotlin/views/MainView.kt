package views

import controllers.ItemController
import javafx.geometry.Pos
import controllers.LoginController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.application.Platform
import models.*
import tornadofx.*
import views.customers.AddCustomerView
import views.customers.CheckView
import views.customers.ChekSelectView
import views.repairWorks.AddRepairWork
import views.workers.AddWorkerView
import views.workers.ReferenceView


class MainView : View("Станция технического обслуживания") {
    val user: UserModel by inject()
    val loginController : LoginController by inject()
    val itemController : ItemController by inject()
    val visible: VisibleModel by inject()

    override val root = vbox(10) {
        setPrefSize(870.0, 620.0)
        alignment = Pos.CENTER
        visible.customerViewIsVisible.value = true
        menubar {
            useMaxWidth = true
            menu("Пользователь ${user.name.value}", FontAwesomeIconView(FontAwesomeIcon.USER)) {
                item("Информация").action {
                    find<InfoUserView> {
                        openModal()
                    }
                }
                separator()
                menu("Выход"){
                    item("Выйти из аккаунта").action(loginController::logout)
                    separator()
                    item("Выйти из программы").action {
                        Platform.exit()
                    }
                }
            }
            menu("Клиенты", FontAwesomeIconView(FontAwesomeIcon.CAR)){
                if(user.level.value.toInt()<3) {
                    item("Добавть клиента").action {
                        find<AddCustomerView> {
                            openModal()
                        }
                    }
                    separator()
                }
                item("Показать список клиентов").action {
                    selectContent(1)
                }
            }
            menu("Персонал", FontAwesomeIconView(FontAwesomeIcon.GROUP)){
                if (user.level.value==1) {
                    item("Нанять работника").action {
                        find<AddWorkerView> {
                            openModal()
                        }
                    }
                    separator()
                }
                item("Показать список работников").action {
                    selectContent(2)
                }
            }
            menu("Склад", FontAwesomeIconView(FontAwesomeIcon.CUBES)){
                item("Добавть запчасть").action {}
                separator()
                item("Показать наличие").action {
                    selectContent(3)
                }
            }
            menu("Работы", FontAwesomeIconView(FontAwesomeIcon.WRENCH)){
                item("Добавть работу").action {
                    find<AddRepairWork> {
                        openModal()
                    }
                }
                separator()
                item("Показать список работ").action {
                    selectContent(4)
                }
            }
            menu("Документы", FontAwesomeIconView(FontAwesomeIcon.FILE_PDF_ALT)){
                item("Чеки").action {
                    find<ChekSelectView>{
                        openModal()
                    }
                }
                separator()
                item("Расходы персонала").action {
                    itemController.refernce()
                    find<ReferenceView> {
                        openModal()
                    }
                }
            }
        }
        add<StackView>()
    }

    fun selectContent(n: Int){
        visible.customerViewIsVisible.value = false
        visible.workerViewIsVisible.value = false
        visible.scladViewIsVisible.value = false
        visible.worksViewIsVisible.value = false
        when(n) {
            1 -> visible.customerViewIsVisible.value = true
            2 -> visible.workerViewIsVisible.value = true
            3 -> visible.scladViewIsVisible.value = true
            4 -> visible.worksViewIsVisible.value = true
        }
    }

}