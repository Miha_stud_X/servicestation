package views.sclad

import controllers.ItemController
import models.ScladTable
import tornadofx.*

class DetailsList : View(){

    val itemController: ItemController by inject()

    override val root = tableview(itemController.itemsS) {
        prefWidth = 420.0
        isEditable = false
        column("Название", ScladTable::name)
        column("Колличесвто", ScladTable::count)
        column("Цена за ед.", ScladTable::cost)
        bindSelected(itemController.selectedItemS)
        smartResize()
    }
}