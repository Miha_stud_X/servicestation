package views.sclad

import models.VisibleModel
import tornadofx.View
import tornadofx.hbox
import tornadofx.vbox
import tornadofx.visibleWhen

class ScladView: View() {
    val visible: VisibleModel by inject()
    override val root = vbox {
        visibleWhen { visible.scladViewIsVisible }
        add<DetailsList>()
    }
}