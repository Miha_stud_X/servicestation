package views

import controllers.LoginController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class LoginView : View("Вход") {
    val model = ViewModel()
    val username = model.bind { SimpleStringProperty() }
    val password = model.bind { SimpleStringProperty() }
    val loginController: LoginController by inject()

    override val root = form {
        fieldset(labelPosition = Orientation.VERTICAL) {
            field("Имя пользователя") {
                textfield(username).required()
            }
            field("Пароль") {
                passwordfield(password).required()
            }
            label(loginController.statusProperty) {
                style {
                    paddingTop = 10
                    paddingBottom = 10
                    textFill = Color.RED
                    fontWeight = FontWeight.BOLD
                }
            }
            button("Войти", FontAwesomeIconView(FontAwesomeIcon.SIGN_IN)) {
                enableWhen(model.valid)
                isDefaultButton = true
                useMaxWidth = true
                action {
                    runAsyncWithProgress {
                        loginController.login(username.value, password.value)
                    }
                }
            }
        }

    }

    override fun onDock() {
        username.value = ""
        password.value = ""
        model.clearDecorators()
    }
}
