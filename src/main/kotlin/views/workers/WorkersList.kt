package views.workers

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import models.UserModel
import models.WorkerTable
import tornadofx.*

class WorkersList : View(){

    val itemController: ItemController by inject()
    val user: UserModel by inject()

    override val root = form {
        val t = tableview(itemController.itemsW) {
            prefWidth = 420.0
            isEditable = true
            column("Фамилия", WorkerTable::surname)
            column("Имя", WorkerTable::name)
            column("Отчество", WorkerTable::potronomic)
            column("Должность", WorkerTable::position)
            bindSelected(itemController.selectedItemW)
            smartResize()
        }
        hbox {
            if (user.level.value.toInt()==1)
            button("Добавить работника", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)).setOnAction {
                find<AddWorkerView> {
                    openModal()
                }
            }
            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)).setOnAction {
                t.refresh()
            }
        }
    }
}