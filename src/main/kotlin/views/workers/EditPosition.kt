package views.workers

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import models.RequestsDB
import tornadofx.*

class EditPosition: View() {

    val selected = SimpleStringProperty()

    override val root = form {
        alignment = Pos.CENTER
        val requestP = RequestsDB()
        val resultP = requestP.getPositions()
        val combox = combobox<String>(selected) {
            val list1 = resultP.map { it.name }
            items.addAll(list1)
        }
        button("Сохранить", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)) {
            action {
                close()
            }
        }
    }
}