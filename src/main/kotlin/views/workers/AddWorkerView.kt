package views.workers

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.geometry.Pos
import models.Position
import models.RequestsDB
import models.WorkerModel
import org.controlsfx.control.Notifications
import tornadofx.*
import kotlin.reflect.KProperty1

class AddWorkerView : View("Найм работника") {
    val model = WorkerModel()
    val selected = SimpleStringProperty()

    override val root = form {
        setPrefSize(350.0, 400.0)
        alignment = Pos.CENTER
        fieldset("Ф.И.О.", FontAwesomeIconView(FontAwesomeIcon.USER)) {
            field("Фамилия") {
                textfield(model.surname).required()
            }

            field("Имя") {
                textfield(model.name).required()
            }

            field("Отчество") {
                textfield(model.potronomic).required()
            }
        }

        fieldset(
            "Персональные данные", FontAwesomeIconView(FontAwesomeIcon.FOLDER_OPEN),
            labelPosition = Orientation.VERTICAL
        ) {
            field("Адрес проживания") {
                textfield(model.addres).required()
            }
        }

        fieldset {
            field("Дата рождения") {
                datepicker(model.date_of_birth).required()
            }
        }

        fieldset(
            "Должность", FontAwesomeIconView(FontAwesomeIcon.RUB),
            labelPosition = Orientation.VERTICAL
        ) {
            field("Специальность") {
                val requestP = RequestsDB()
                val resultP  = requestP.getPositions()
                combobox<String>(selected) {
                    val list1 = resultP.map { it.name}
                    items.addAll(list1)
                }
            }
        }
        fieldset{
            field {
                button("Нанять", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)) {
                    action {
                        model.commit {
                            val warker = model.item
                            warker.position = selected.value
                            val request = RequestsDB()
                            request.addWorker(warker)
                            Notifications.create()
                                .title("Работник нанят!")
                                .text("${warker.surname} ${warker.name} ${warker.potronomic}\nна должность ${warker.position}")
                                .owner(this)
                                .showInformation()
                        }
                    }

                    enableWhen(model.valid)
                }
            }
            field {
                button("Закрыть", FontAwesomeIconView(FontAwesomeIcon.CLOSE)) {
                    action {
                        close()
                    }
                }
            }
        }
    }

}