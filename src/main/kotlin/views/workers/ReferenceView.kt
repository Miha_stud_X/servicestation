package views.workers

import javafx.geometry.Pos
import tornadofx.View
import tornadofx.vbox

class ReferenceView: View("Чек") {
    override val root = vbox {
        prefWidth = 480.0
        alignment = Pos.CENTER
        add<ReferenceList>()
    }
}