package views.workers

import controllers.ItemController
import models.Chek
import models.Reference
import tornadofx.*

class ReferenceList: View() {

    val itemController: ItemController by inject()

    override val root = tableview(itemController.itemsReference){
        isEditable = false
        column("Фамилия", Reference::surname)
        column("имя", Reference::name)
        column("Отчество", Reference::potronymic)
        column("Детали", Reference::detail)
        column("Колличество", Reference::count)
        smartResize()
    }
}