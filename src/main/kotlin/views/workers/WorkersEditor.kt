package views.workers

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.geometry.Orientation
import javafx.scene.text.FontWeight
import models.RequestsDB
import models.UserModel
import models.VisibleModel
import org.controlsfx.control.Notifications
import tornadofx.*
import views.repairWorks.WorksCustomerWin

class WorkersEditor : View() {

    val itemController: ItemController by inject()
    val requestW = RequestsDB()
    val user: UserModel by inject()

    override val root = form {

        fieldset("Ф.И.О.", FontAwesomeIconView(FontAwesomeIcon.USER), labelPosition = Orientation.VERTICAL) {
            field("Фамилия") {
                if (user.level.value.toInt()==1)
                    textfield(itemController.selectedItemW.surname).required()
                else
                    textfield(itemController.selectedItemW.surname){
                        setDisable(true)
                    }
            }

            field("Имя") {
                if (user.level.value.toInt()==1)
                    textfield(itemController.selectedItemW.name).required()
                else
                    textfield(itemController.selectedItemW.name){
                        setDisable(true)
                    }
            }

            field("Отчество") {
                if (user.level.value.toInt()==1)
                    textfield(itemController.selectedItemW.potronomic).required()
                else
                    textfield(itemController.selectedItemW.potronomic){
                        setDisable(true)
                    }
            }
        }

        fieldset("Персональные данные", FontAwesomeIconView(FontAwesomeIcon.FOLDER_OPEN),
            labelPosition = Orientation.VERTICAL
        ) {
            field("Адрес") {
                if (user.level.value.toInt()==1)
                    textfield(itemController.selectedItemW.addres).required()
                else
                    textfield(itemController.selectedItemW.addres){
                        setDisable(true)
                    }
            }

            field("Дата рождения") {
                if (user.level.value.toInt()==1)
                    datepicker(itemController.selectedItemW.date_of_birth).required()
                else
                    datepicker(itemController.selectedItemW.date_of_birth){
                        setDisable(true)
                    }
            }
        }

        fieldset("Должность", FontAwesomeIconView(FontAwesomeIcon.RUB), labelPosition = Orientation.VERTICAL) {
            field("Специальность и Заработная плата") {
                textfield(itemController.selectedItemW.position){
                    setDisable(true)
                }
                val tf2 = textfield (itemController.selectedItemW.salary)
                tf2.setDisable(true)
            }
        }


        fieldset("Действия", FontAwesomeIconView(FontAwesomeIcon.EDIT)) {

            if (user.level.value.toInt()==1)
            field {
                button("Сохранить") {
                    action {
                        itemController.selectedItemW.commit {
                            val worker = itemController.selectedItemW.item
                            requestW.setWorkers(worker)
                            Notifications.create()
                                .title("Работник изменён!")
                                .text("${worker.surname} ${worker.name} ${worker.potronomic}\nна должности ${worker.position}")
                                .owner(this)
                                .showInformation()
                        }
                    }
                    enableWhen(itemController.selectedItemW.valid)
                }

                button("Уволить") {
                    action {
                        itemController.selectedItemW.commit {
                            val worker = itemController.selectedItemW.item
                            requestW.delWarker(worker)
                            itemController.itemsW.remove(worker)
                            Notifications.create()
                                .title("Работник уволен!")
                                .text("${worker.surname} ${worker.name} ${worker.potronomic}\nна должности ${worker.position}")
                                .owner(this)
                                .showInformation()
                        }
                    }
                    enableWhen(itemController.selectedItemW.valid)
                }
            }
            field {
                button("Просмотреть работы") {
                    action {
                        itemController.reapairWorksWorker(itemController.selectedItemW.id.value)
                        find<WorksCustomerWin> {
                            openModal()
                        }
                    }
                    enableWhen(itemController.selectedItemW.valid)
                }
                if (user.level.value.toInt()==1)
                button("Изменить должность") {
                    action {
                        find<EditPosition> {
                            openModal()
                        }
                    }
                    enableWhen(itemController.selectedItemW.valid)
                }
            }
        }
    }
}