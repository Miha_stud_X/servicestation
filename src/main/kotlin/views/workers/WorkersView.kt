package views.workers

import models.VisibleModel
import tornadofx.View
import tornadofx.hbox
import tornadofx.visibleWhen

class WorkersView: View() {
    val visible: VisibleModel by inject()
    override val root = hbox {
        visibleWhen { visible.workerViewIsVisible }
        add<WorkersList>()
        add<WorkersEditor>()
    }
}