package views

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.geometry.Orientation
import javafx.scene.text.FontWeight
import models.UserModel
import tornadofx.*

class InfoUserView : View("Информация") {
    val user: UserModel by inject()

    override val root =  form {
        fieldset("Информация о пользователе", FontAwesomeIconView(FontAwesomeIcon.INFO),
            labelPosition = Orientation.HORIZONTAL) {
            field("Пользователь:") {
                label(user.name.value){
                    style {
                        fontWeight = FontWeight.EXTRA_BOLD
                        fontSize = 30.px
                    }
                }
            }
            field("Уровень доступа:") {
                label(user.level.value.toString()){
                    style {
                        fontWeight = FontWeight.EXTRA_BOLD
                        fontSize = 30.px
                    }
                }
            }
            button("Ок", FontAwesomeIconView(FontAwesomeIcon.CHECK)).setOnAction {
                close()
            }
        }
    }
}