package views.customers

import controllers.ItemController
import models.Chek
import tornadofx.*

class ChekList: View() {

    val itemController: ItemController by inject()

    override val root = tableview(itemController.itemsChek){
        isEditable = false
        column("Детали", Chek::detail)
        column("Цена за ед.", Chek::cost)
        column("Колличесвто", Chek::count)
        column("Сумма", Chek::sum)
        smartResize()
    }
}