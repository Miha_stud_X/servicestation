package views.customers

import javafx.geometry.Pos
import tornadofx.View
import tornadofx.vbox

class CheckView: View("Чек") {
    override val root = vbox {
        prefWidth = 480.0
        alignment = Pos.CENTER
        add<ChekList>()
    }
}