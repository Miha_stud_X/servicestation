package views.customers

import controllers.ItemController
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

class ChekSelectView : View("Выбор клиента") {

    val itemController: ItemController by inject()
    val selected = SimpleStringProperty()

    override val root = form {
        fieldset{
            field("Клиент") {
                combobox<String>(selected) {
                    items.addAll(itemController.itemsC.map { it.id.toString()+": "+it.surname+" "+it.name+" "+it.potronomic})
                }
            }
            field{
                button("Продолжить"){  action {
                    itemController.chek(selected.value.substring(0, 1).toInt())
                    find<CheckView> {
                        openModal()
                    }

                }}
            }
        }
    }
}