package views.customers

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import models.CustomerModel
import models.CustomerTable
import models.UserModel
import tornadofx.*
import javax.swing.text.TableView

class CustomersList : View() {
    val itemController: ItemController by inject()
    val user: UserModel by inject()

    override val root = form {

        val t = tableview(itemController.itemsC) {
            prefWidth = 420.0
            isEditable = true
            //column("Фамилия", CustomerTable::surname)
            column("Имя", CustomerTable::name)
            column("Отчество", CustomerTable::potronomic)
            column("Авто", CustomerTable::car)
            column("Номер", CustomerTable::car_badge)
            bindSelected(itemController.selectedItemC)
            smartResize()
        }
        hbox {
            if (user.level.value.toInt()<3)
            button("Добавить клиента", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)).setOnAction {
                find<AddCustomerView> {
                    openModal()
                }
            }
            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)).setOnAction {
                t.refresh()
            }
        }
    }
}