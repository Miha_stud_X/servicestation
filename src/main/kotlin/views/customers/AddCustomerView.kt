package views.customers

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.property.Property
import javafx.event.EventTarget
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.DatePicker
import org.controlsfx.control.Notifications
import models.CustomerModel
import models.RequestsDB
import tornadofx.*
import tornadofx.control.DateTimePicker
import views.MainView
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.system.exitProcess

class AddCustomerView : View("Добавление клиента") {

    val model = CustomerModel()
    val itemController: ItemController by inject()

    override val root = form {
        setPrefSize(400.0, 500.0)
        alignment = Pos.CENTER
        fieldset("Ф.И.О.", FontAwesomeIconView(FontAwesomeIcon.USER)) {
            field("Фамилия") {
                textfield(model.surname).required()
            }

            field("Имя") {
                textfield(model.name).required()
            }

            field("Отчество") {
                textfield(model.potronomic).required()
            }
        }

        fieldset("Контакты", FontAwesomeIconView(FontAwesomeIcon.PHONE)) {
            field("Номер телефона") {
                textfield(model.phone).required()
            }
        }

        fieldset("Автомобиль", FontAwesomeIconView(FontAwesomeIcon.CAR)) {
            field("Модель") {
                textfield(model.car).required()
            }

            field("Номерной знак") {
                textfield(model.car_budge).required()
            }
        }

        fieldset("Даты", FontAwesomeIconView(FontAwesomeIcon.CALENDAR)) {
            field("Начало работ") {
                val dateTime1 = DateTimePicker()
                dateTime1.dateTimeValueProperty().bindBidirectional(model.timeStart)
                add(dateTime1)
            }
            field("Планируемое окончание работ") {
                val dateTime2 = DateTimePicker()
                dateTime2.dateTimeValueProperty().bindBidirectional(model.timePlane)
                add(dateTime2)
            }
        }
        fieldset{
            field {
                button("Добавить", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)) {
                    action {
                        model.commit {
                            val customer = model.item
                            val request = RequestsDB()
                            request.addCustomers(customer)
                            itemController.itemsC.addAll(customer)
                            Notifications.create()
                                .title("Клиент добавлен!")
                                .text("${customer.surname} ${customer.name} ${customer.potronomic}\nна автомобиле ${customer.car} ${customer.car_badge}")
                                .owner(this)
                                .showInformation()
                        }
                    }
                    enableWhen(model.valid)
                }
            }
            field {
                button("Закрыть", FontAwesomeIconView(FontAwesomeIcon.CLOSE)) {
                    action {
                        close()
                    }
                }
            }
        }
    }
}

//    fun EventTarget.datetimepicker(property: Property<LocalDateTime>, op: DatePicker.() -> Unit = {}) = DateTimePicker().apply {
//        bind(property)
//        op(this)
//    }
