package views.customers

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.collections.FXCollections
import javafx.geometry.Orientation
import models.RepairWorkTable
import models.RequestsDB
import models.UserModel
import org.controlsfx.control.Notifications
import tornadofx.*
import tornadofx.control.DateTimePicker
import views.repairWorks.WorksCustomerWin

class CustomerEditor : View() {

    val itemController: ItemController by inject()
    val requestSetC = RequestsDB()
    val dateTime = DateTimePicker()
    val user: UserModel by inject()

    override val root = form {
        //visibleWhen { itemController.selectedItemW.name.isBlank()}
        fieldset("Ф.И.О.", FontAwesomeIconView(FontAwesomeIcon.USER), labelPosition = Orientation.VERTICAL) {
            field("Фамилия") {
                textfield(itemController.selectedItemC.surname).required()
            }

            field("Имя") {
                textfield(itemController.selectedItemC.name).required()
            }

            field("Отчество") {
                textfield(itemController.selectedItemC.potronomic).required()
            }

            field("Номер телефона") {
                textfield(itemController.selectedItemC.phone).required()
            }
        }

        fieldset("Автомобиль", FontAwesomeIconView(FontAwesomeIcon.CAR), labelPosition = Orientation.VERTICAL) {
            field("Модель и номерной знак") {
                textfield(itemController.selectedItemC.car).required()
                textfield(itemController.selectedItemC.car_budge).required()
            }
        }

        fieldset("Даты", FontAwesomeIconView(FontAwesomeIcon.CALENDAR)) {
            field("Начало работ") {
                dateTime.dateTimeValueProperty().bindBidirectional(itemController.selectedItemC.timeStart)
                add(dateTime)
            }
            field("Планируемое окончание работ") {
                val timePlane = DateTimePicker()
                timePlane.dateTimeValueProperty().bindBidirectional(itemController.selectedItemC.timePlane)
                add(timePlane)
            }
            field("Фактическое окончание работ") {
                val timeEnd = DateTimePicker()
                timeEnd.dateTimeValueProperty().bindBidirectional(itemController.selectedItemC.timeEnd)
                add(timeEnd)
            }
            field {
                button("Сохранить") {
                    action {
                        itemController.selectedItemC.commit {
                           val customer = itemController.selectedItemC.item
                            requestSetC.setCustomers(customer)
                            Notifications.create()
                                .title("Клиент изменён!")
                                .text("${customer.surname} ${customer.name} ${customer.potronomic}\nна автомобиле ${customer.car} ${customer.car_badge}")
                                .owner(this)
                                .showInformation()
                        }
                    }
                    enableWhen(itemController.selectedItemC.valid)
                }
                button("Просмотреть работы") {
                    action {
                        itemController.reapairWorksCustomer(itemController.selectedItemC.id.value)
                        find<WorksCustomerWin> {
                            openModal()
                        }
                    }
                    enableWhen(itemController.selectedItemC.valid)
                }
                if (user.level.value.toInt()<3)
                button("Удалить") {
                    action {
                        val customer = itemController.selectedItemC.item
                        requestSetC.delCustomer(customer)
                        itemController.itemsC.remove(customer)
                    }
                    enableWhen(itemController.selectedItemC.valid)
                }
            }
        }

        onDockListeners
    }


}