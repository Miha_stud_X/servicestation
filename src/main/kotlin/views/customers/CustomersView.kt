package views.customers

import models.VisibleModel
import tornadofx.View
import tornadofx.hbox
import tornadofx.visibleWhen

class CustomersView : View() {
    val visible: VisibleModel by inject()
    override val root = hbox {
       visibleWhen { visible.customerViewIsVisible}
       add<CustomersList>()
       add<CustomerEditor>()
    }
}