package views.repairWorks

import controllers.ItemController
import models.RepairWorkTable
import tornadofx.*

class WorksCustomerWin: View() {

    val itemController: ItemController by inject()

    override val root = form {
        tableview(itemController.itemsRWC) {
            prefWidth = 420.0
            isEditable = false
            column("Автомобиль", RepairWorkTable::customer_car)
            column("Номер", RepairWorkTable::customer_car_budge)
            column("Повреждение", RepairWorkTable::damage)
            column("Мастер", RepairWorkTable::worker)
            column("Статус", RepairWorkTable::status)
            bindSelected(itemController.selectedItemRWC)
            smartResize()
        }
    }
}