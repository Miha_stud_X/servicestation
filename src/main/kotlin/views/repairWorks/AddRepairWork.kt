package views.repairWorks

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import models.RepairWork
import models.RepairWorkModel
import models.RequestsDB
import org.controlsfx.control.Notifications
import tornadofx.*

class AddRepairWork: View("Добавить ремонтную работу") {

    val model = RepairWorkModel()
    val selectedC = SimpleStringProperty()
    val selectedM = SimpleStringProperty()
    val selectedN = SimpleStringProperty()
    val itemController: ItemController by inject()

    override val root = form {

        fieldset(
            "Ремонтная работа", FontAwesomeIconView(FontAwesomeIcon.WRENCH),
            labelPosition = Orientation.VERTICAL
        ) {
            field("Автомобиль") {
                combobox<String>(selectedC) {
                    items.addAll(itemController.itemsC.map { it.car+" "+it.car_badge})
                }
            }
            field("Мастер") {
                combobox<String>(selectedM) {
                    items.addAll(itemController.itemsW.map { it.surname+" "+it.name[1]+". "+it.potronomic[1]})
                }
            }
            field("Неполадка") {
                combobox<String>(selectedN) {
                    val requestD = RequestsDB()
                    val resultD  = requestD.getDamages()
                    items.addAll(resultD.map { it.name})
                }
            }
            field("Работа") {
                combobox<String>(selectedN) {
                    val requestS = RequestsDB()
                    val resultS  = requestS.getService()
                    items.addAll(resultS.map { it.name+" "+it.selery.toString()})
                }
            }
        }
        fieldset{
            field {
                button("Приступить", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)) {
                    action {
                        model.commit {
                            val wark = model.item
                            wark.customer_car = selectedC.value
                            wark.worker = selectedM.value
                            wark.damage = selectedN.value
                            val request = RequestsDB()
                            //request.addWork(wark)
                            Notifications.create()
                                .title("Работ добавлена!")
                                //.text("${warker.surname} ${warker.name} ${warker.potronomic}\nна должность ${warker.position}")
                                .owner(this)
                                .showInformation()
                        }
                    }

                    enableWhen(model.valid)
                }
            }
            field {
                button("Закрыть", FontAwesomeIconView(FontAwesomeIcon.CLOSE)) {
                    action {
                        close()
                    }
                }
            }
        }
    }
}