package views.repairWorks

import controllers.ItemController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import models.RepairWorkTable
import tornadofx.*
import views.customers.AddCustomerView
import views.workers.AddWorkerView

class RepairWorksList : View(){
    val itemController: ItemController by inject()

    override val root = form{
        val t = tableview(itemController.itemsRW) {
            prefWidth = 420.0
            isEditable = false
            column("Автомобиль", RepairWorkTable::customer_car)
            column("Номер", RepairWorkTable::customer_car_budge)
            column("Повреждение", RepairWorkTable::damage)
            column("Мастер", RepairWorkTable::worker)
            column("Статус", RepairWorkTable::status)
            bindSelected(itemController.selectedItemRW)
            smartResize()
        }
        hbox {
            button("Добавить ремонт", FontAwesomeIconView(FontAwesomeIcon.USER_PLUS)).setOnAction {

            }
            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)).setOnAction {
                t.refresh()
            }
        }
    }

}