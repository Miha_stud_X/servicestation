package views.repairWorks

import models.VisibleModel
import tornadofx.View
import tornadofx.vbox
import tornadofx.visibleWhen

class RepairWorksView : View() {
    val visible: VisibleModel by inject()
    override val root = vbox {
        visibleWhen { visible.worksViewIsVisible }
        add<RepairWorksList>()
    }
}