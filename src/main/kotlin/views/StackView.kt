package views

import tornadofx.*
import views.customers.CustomersView
import views.repairWorks.RepairWorksView
import views.sclad.ScladView
import views.workers.WorkersView

class StackView : View() {

    override val root =  stackpane {
        add<CustomersView>()
        add<WorkersView>()
        add<ScladView>()
        add<RepairWorksView>()
    }
}
