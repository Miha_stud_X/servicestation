package models

import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import tornadofx.ItemViewModel
import tornadofx.getProperty
import tornadofx.property
import java.sql.Date
import java.time.LocalDate

class WorkerTable {
    var id by property<Int>()
    fun idProperty() = getProperty(WorkerTable::id)

    var surname by property<String>()
    fun surnameProperty() = getProperty(WorkerTable::surname)

    var name by property<String>()
    fun nameProperty() = getProperty(WorkerTable::name)

    var potronomic by property<String>()
    fun potronomicProperty() = getProperty(WorkerTable::potronomic)

    var addres by property<String>()
    fun addresProperty() = getProperty(WorkerTable::addres)

    var date_of_birth by property<LocalDate>()
    fun date_of_birthProperty() = getProperty(WorkerTable::date_of_birth)

    var position by property<String>()
    fun positionProperty() = getProperty(WorkerTable::position)

    var salary by property<Number>()
    fun salaryProperty() = getProperty(WorkerTable::salary)

    var id_position by property<Int>()
    fun id_positionProperty() = getProperty(WorkerTable::id_position)

    override fun toString() = name
}

class Worker(val id:Int, val surname: String, val name : String, val potronomic: String, val addres:String,
               val date_of_birth: Date, val position: String, val salary : Float, val id_position: Int)

class WorkerModel : ItemViewModel<WorkerTable>(WorkerTable()) {
    val id: Property<Int> = bind { item?.idProperty() }
    val surname: StringProperty = bind { item?.surnameProperty() }
    val name: StringProperty = bind { item?.nameProperty() }
    val potronomic: StringProperty = bind { item?.potronomicProperty() }
    val addres: StringProperty = bind { item.addresProperty() }
    val date_of_birth: Property<LocalDate> = bind { item?.date_of_birthProperty() }
    val position: StringProperty = bind { item?.positionProperty() }
    val salary: Property<Number> = bind { item?.salaryProperty() }
    val id_position: Property<Int> = bind { item?.id_positionProperty() }
}