package models

import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import tornadofx.ItemViewModel
import tornadofx.getProperty
import tornadofx.property

class ScladTable {
    var id by property<Int>()
    fun idProperty() = getProperty(ScladTable::id)

    var name by property<String>()
    fun nameProperty() = getProperty(ScladTable::name)

    var count by property<String>()
    fun countProperty() = getProperty(ScladTable::count)

    var cost by property<Float>()
    fun costProperty() = getProperty(ScladTable::cost)
}

class Sclad(val id:Int, val name : String, val count: String, val cost : Float)

class ScladModel : ItemViewModel<ScladTable>(ScladTable()) {
    val id: Property<Int> = bind { item?.idProperty() }
    val name: StringProperty = bind { item?.nameProperty() }
    val count: StringProperty = bind { item?.countProperty() }
    val cost: Property<Float> = bind { item?.costProperty() }
}