package models

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

class User{
    val nameProperty = SimpleStringProperty()
    val levelProperty = SimpleIntegerProperty()
    var name by nameProperty
    var level by levelProperty

    /*
    fun updateModelUser(name_user: String, user_level: Int) {
        name = name_user
        level = user_level
    }*/
}

class UserModel : ItemViewModel<User>() {
    val name = bind(User::nameProperty)
    val level = bind(User::levelProperty)
}