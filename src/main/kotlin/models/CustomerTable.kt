package models

import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import javafx.util.converter.TimeStringConverter
import java.time.LocalDate
import tornadofx.*
import java.sql.Date
import java.sql.Timestamp
import java.time.LocalDateTime


class CustomerTable {

    var id by property<Int>()
    fun idProperty() = getProperty(CustomerTable::id)

    var surname by property<String>()
    fun surnameProperty() = getProperty(CustomerTable::surname)

    var name by property<String>()
    fun nameProperty() = getProperty(CustomerTable::name)

    var potronomic by property<String>()
    fun potronomicProperty() = getProperty(CustomerTable::potronomic)

    var phone by property<String>()
    fun phomeProperty() = getProperty(CustomerTable::phone)

    var dateStart by property<LocalDate>()
    fun dateStartPropery() = getProperty(CustomerTable::dateStart)

    var timeStart by property<LocalDateTime>()
    fun timeStartProperty() = getProperty(CustomerTable::timeStart)

    var car by property<String>()
    fun carProperty() = getProperty(CustomerTable::car)

    var car_badge by property<String>()
    fun car_badgeProperty() = getProperty(CustomerTable::car_badge)

    var timePlane by property<LocalDateTime>()
    fun timePlaneProperty() = getProperty(CustomerTable::timePlane)

    var timeEnd by property<LocalDateTime>()
    fun timeEndProperty() = getProperty(CustomerTable::timeEnd)

    override fun toString() = name
}


class Customer(val id:Int, val surname: String, val name : String, val potronomic: String, val phone:String,
               val timeStart: java.sql.Timestamp, val car: String, val car_badge : String, val timePlane: java.sql.Timestamp,
               val timeEnd: java.sql.Timestamp)


class CustomerModel : ItemViewModel<CustomerTable>(CustomerTable()) {
    val id: Property<Int> = bind { item?.idProperty() }
    val surname: StringProperty = bind { item?.surnameProperty() }
    val name: StringProperty = bind { item?.nameProperty() }
    val potronomic: StringProperty = bind { item?.potronomicProperty() }
    val phone: StringProperty = bind { item.phomeProperty() }
    val timeStart: Property<LocalDateTime> = bind {item?.timeStartProperty()}
    val car: StringProperty = bind { item?.carProperty() }
    val car_budge: StringProperty = bind { item?.car_badgeProperty() }
    val timePlane: Property<LocalDateTime> = bind { item?.timePlaneProperty() }
    val timeEnd: Property<LocalDateTime> = bind { item?.timeEndProperty() }
}