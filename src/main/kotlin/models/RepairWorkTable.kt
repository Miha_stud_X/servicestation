package models

import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import tornadofx.ItemViewModel
import tornadofx.getProperty
import tornadofx.property

class RepairWorkTable {
    var id_work by property<Int>()
    fun id_workProperty() = getProperty(RepairWorkTable::id_work)

    var id_customer by property<Int>()
    fun id_customerProperty() = getProperty(RepairWorkTable::id_customer)

    var id_employee by property<Int>()
    fun id_employeeProperty() = getProperty(RepairWorkTable::id_employee)

    var customer_car by property<String>()
    fun customer_carProperty() = getProperty(RepairWorkTable::customer_car)

    var customer_car_budge by property<String>()
    fun customer_car_budgeProperty() = getProperty(RepairWorkTable::customer_car_budge)

    var damage by property<String>()
    fun damageProperty() = getProperty(RepairWorkTable::damage)

    var worker by property<String>()
    fun workerProperty() = getProperty(RepairWorkTable::worker)

    var status by property<String>()
    fun statusProperty() = getProperty(RepairWorkTable::status)

}

class RepairWork(val id_work:Int, val id_customer:Int, val customer_car: String, val customer_car_budge : String,
                 val damage: String, val id_employee: Int, val worker:String, val status: String)

class RepairWorkModel : ItemViewModel<RepairWorkTable>(RepairWorkTable()) {
    val id_work: Property<Int> = bind { item?.id_workProperty() }
    val id_customer: Property<Int> = bind { item?.id_customerProperty() }
    val id_employee: Property<Int> = bind { item?.id_employeeProperty() }
    val customer_car: StringProperty = bind { item?.customer_carProperty() }
    val customer_car_budge: StringProperty = bind { item?.customer_car_budgeProperty() }
    val damage: StringProperty = bind { item?.damageProperty() }
    val worker: StringProperty = bind { item.workerProperty() }
    val status: Property<String> = bind { item?.statusProperty() }
}