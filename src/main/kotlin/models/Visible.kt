package models

import javafx.beans.property.SimpleBooleanProperty
import tornadofx.ItemViewModel

class Visible {
   val customeViewProperty = SimpleBooleanProperty()
    val workerViewProperty = SimpleBooleanProperty()
    val scladViewProperty = SimpleBooleanProperty()
    val worksViewProperty = SimpleBooleanProperty()
    //val viewEditCProperty = SimpleBooleanProperty()
    //val viewEditWProperty = SimpleBooleanProperty()
    //val viewEditSProperty = SimpleBooleanProperty()

}

class VisibleModel : ItemViewModel<Visible>() {
    val customerViewIsVisible = bind(Visible::customeViewProperty)
    val workerViewIsVisible = bind(Visible::workerViewProperty)
    val scladViewIsVisible = bind(Visible::scladViewProperty)
    val worksViewIsVisible = bind(Visible::worksViewProperty)
    //val viewEditCIsVisible = bind(Visible::viewEditCProperty)
    //val viewEditWIsVisible = bind(Visible::viewEditWProperty)
    //val viewEditSIsVisible = bind(Visible::viewEditSProperty)
}