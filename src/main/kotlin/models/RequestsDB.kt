package models

import com.github.davidmoten.rx.jdbc.Database
import kotlin.math.absoluteValue

class RequestsDB {

    val db = Database.from(
        "jdbc:mysql://${SettingsDatabase.Host.value}/" +
                "${SettingsDatabase.Database.value}?characterEncoding=UTF-8",
        SettingsDatabase.User.value, SettingsDatabase.Password.value
    )

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val requestsDB = RequestsDB()
            requestsDB.getCustomers()
            println()
        }
    }

    fun getUsers(): List<User_db> {
        return db.select("select user_login, user_password, user_properties_level from users")
            .autoMap(User_db::class.java).toList().toBlocking().single()
    }

    fun getCustomers(): List<CustomerTable> {
        return db.select("select * from customers")
            .autoMap(Customer::class.java).map{ transformC(it) }.toList().toBlocking().single()
    }

    fun getWorkers(): List<WorkerTable> {
        return db.select("select " +
                "id_employee, employee_surname, employee_name, employee_potronymic, " +
                "employee_address, employee_date_of_birth, position_name, position_salary, e.id_position " +
                "from employees e " +
                "inner join positions p " +
                "ON p.id_position = e.id_position")
            .autoMap(Worker::class.java).map{ transformW(it) }.toList().toBlocking().single()
    }

    fun getDetails(): List<ScladTable>{
        return db.select("select id_detail, detail_name, CONCAT(detail_count,unit), detail_cost " +
                "from details d inner join units u on d.id_unit = u.id_unit")
            .autoMap(Sclad::class.java).map{ transformS(it) }.toList().toBlocking().single()
    }

    fun getReapairWorks(): List<RepairWorkTable>{
        return db.select("select r.id_work, r.id_customer, customer_car, customer_car_badge, damage, " +
                "r.id_employee, CONCAT(employee_surname, \" \", LEFT(employee_potronymic,1), \". \", " +
                "LEFT(employee_name,1), \". \", position_name), status " +
                "from repair_works r " +
                "inner join customers c on r.id_customer = c.id_customer " +
                "inner join employees e on r.id_employee = e.id_employee " +
                "inner join positions p on e.id_position = p.id_position " +
                "inner join damages d on r.id_damage = d.id_damage " +
                "inner join statuses st on r.id_status = st.id_status")
            .autoMap(RepairWork::class.java).map{ transformRW(it) }.toList().toBlocking().single()
    }

    fun getReapairWorksCustomer(id_cusromer: Int): List<RepairWorkTable>{
        return db.select("select r.id_work, r.id_customer, customer_car, customer_car_badge, damage, " +
                "r.id_employee, CONCAT(employee_surname, \" \", LEFT(employee_potronymic,1), \". \", " +
                "LEFT(employee_name,1), \". \", position_name), status " +
                "from repair_works r " +
                "inner join customers c on r.id_customer = c.id_customer " +
                "inner join employees e on r.id_employee = e.id_employee " +
                "inner join positions p on e.id_position = p.id_position " +
                "inner join damages d on r.id_damage = d.id_damage " +
                "inner join statuses st on r.id_status = st.id_status " +
                "where r.id_customer = $id_cusromer")
            .autoMap(RepairWork::class.java).map{ transformRW(it) }.toList().toBlocking().single()
    }

    fun getReapairWorksWorker(id_worker: Int): List<RepairWorkTable>{
        return db.select("select r.id_work, r.id_customer, customer_car, customer_car_badge, damage, " +
                "r.id_employee, CONCAT(employee_surname, \" \", LEFT(employee_potronymic,1), \". \", " +
                "LEFT(employee_name,1), \". \", position_name), status " +
                "from repair_works r " +
                "inner join customers c on r.id_customer = c.id_customer " +
                "inner join employees e on r.id_employee = e.id_employee " +
                "inner join positions p on e.id_position = p.id_position " +
                "inner join damages d on r.id_damage = d.id_damage " +
                "inner join statuses st on r.id_status = st.id_status " +
                "where r.id_employee = $id_worker")
            .autoMap(RepairWork::class.java).map{ transformRW(it) }.toList().toBlocking().single()
    }

    fun getPassword(login: String, password: String): Int {
        val resultList: List<User_properties> =
            db.select("select user_properties_level from users u where u.user_login='$login' and u.user_password='$password'")
                .autoMap(User_properties::class.java).toList().toBlocking().single()
        return if (resultList.isEmpty())
            0
        else
            resultList[0].properties_level
    }

    fun setCustomers(model: CustomerTable) {
        val s: String = "update customers c set c.customer_surname = '${model.surname}', " +
                "c.customer_nam='${model.name}', c.customer_potronymic = '${model.potronomic}', " +
                "c.customer_phone='${model.phone}', c.customer_car='${model.car}', " +
                "c.customer_car_badge='${model.car_badge}', customer_date_and_time='${model.timeStart}', " +
                "c.customer_planned_time='${model.timePlane}', c.customer_actual_time='${model.timeEnd}' " +
                "where c.id_customer = '${model.id}'"
        db.update(s).execute()
    }

    fun addCustomers(model: CustomerTable) {
        val s: String = "INSERT INTO customers(customer_surname, customer_nam, customer_potronymic, " +
                "customer_phone, customer_car, customer_car_badge) VALUES('${model.surname}', '${model.name}', " +
                "'${model.potronomic}', '${model.phone}', '${model.car}', '${model.car_badge}')"
        db.update(s).execute()
    }

    fun delCustomer(model: CustomerTable){
        db.update("delete from customers where customer_nam ='${model.name}' and customer_surname='${model.surname}'").execute()
    }

    fun delWarker(model: WorkerTable){
        db.update("delete from employees where id_employee = '${model.id}'").execute()
    }

    fun setWorkers(model: WorkerTable) {
        db.update("update employees e set e.employee_surname = '${model.surname}', " +
                "e.employee_name='${model.name}', e.employee_potronymic = '${model.potronomic}', " +
                "e.employee_address='${model.addres}', e.employee_date_of_birth='${model.date_of_birth}' " +
                "where e.id_employee = '${model.id}'").execute()
    }

    fun addWorker(model: WorkerTable) {
        val nPos = db.select("select id_position from positions where position_name ='${model.position}'").autoMap(nPosition::class.java).toList().toBlocking().single()
        val s: String = "INSERT INTO employees (employee_surname, employee_name, employee_potronymic, " +
                "employee_address, employee_date_of_birth, id_position) VALUES('${model.surname}', '${model.name}', " +
                "'${model.potronomic}', '${model.addres}', STR_TO_DATE('${model.date_of_birth}', '%Y-%m-%d'), ${nPos[0].n.absoluteValue})"
        db.update(s).execute()
    }

    fun getPositions(): List<Position>{
        return db.select("select id_position, position_name, position_salary from positions")
            .autoMap(Position::class.java).toList().toBlocking().single()
    }

    fun getCustomersStr(): List<Car>{
        return db.select("select customer_car, customer_car_badge")
            .autoMap(Car::class.java).toList().toBlocking().single()
    }

    fun getDamages(): List<Damage>{
        return db.select("select id_damage, damage from damages")
            .autoMap(Damage::class.java).toList().toBlocking().single()
    }

    fun getService(): List<Service>{
        return db.select("select id_service, service_name, service_cost from services")
            .autoMap(Service::class.java).toList().toBlocking().single()
    }

    fun getChek(id_cutomer: Int): List<Chek>{
        return db.select("SELECT detail, cost, count, sum " +
                "FROM check_to_employees " +
                "WHERE id_customer=$id_cutomer " +
                "UNION ALL " +
                "SELECT ' ',' ','Итого :', SUM(с.sum) " +
                "FROM check_to_employees с " +
                "WHERE id_customer=$id_cutomer")
            .autoMap(Chek::class.java).toList().toBlocking().single()
    }

    fun getRefefence(): List<Reference>{
        return db.select("SELECT * FROM reference_to_workers;")
            .autoMap(Reference::class.java).toList().toBlocking().single()
    }

    fun transformC(it: Customer) = CustomerTable().apply {
        id = it.id
        surname = it.surname
        name = it.name
        potronomic = it.potronomic
        phone = it.phone
        timeStart = it.timeStart.toLocalDateTime()
        car = it.car
        car_badge = it.car_badge
        timePlane = it.timePlane.toLocalDateTime()
        timeEnd = it.timeEnd.toLocalDateTime()
    }

    fun transformW(it: Worker) = WorkerTable().apply {
        id = it.id
        surname = it.surname
        name = it.name
        potronomic = it.potronomic
        addres = it.addres
        date_of_birth = it.date_of_birth.toLocalDate()
        position = it.position
        salary = it.salary
    }

    fun transformS(it: Sclad) = ScladTable().apply {
        id = it.id
        name = it.name
        count = it.count
        cost = it.cost
    }

    fun transformRW(it: RepairWork) = RepairWorkTable().apply {
        id_work = it.id_work
        id_customer = it.id_customer
        id_employee = it.id_employee
        customer_car = it.customer_car
        customer_car_budge = it.customer_car_budge
        damage = it.damage
        worker = it.worker
        status = it.status
    }
}

data class User_db(val login: String, val password: String, val properties_level: Int)
data class User_properties(val properties_level: Int)
/*
data class Customer_db(val surname: String, val name : String, val potronomic: String, val phone:String,
                       val dateStert: Date, val car: String, val car_badge : String, val datePlane: Date, val dateEnd: Date)
data class Employee(val login: String, val password: String)
data class Position(val login: String, val password: String)
data class Service(val login: String, val password: String)
data class Specialization(val login: String, val password: String)
data class MoveOrder(val login: String, val password: String)
data class CustomerTable(val login: String, val password: String)
data class RepairWorks(val login: String, val password: String)
data class Damage(val login: String, val password: String)
data class Unit(val login: String, val password: String)
data class Status(val login: String, val password: String)
data class Spare(val login: String, val password: String)
data class Detail(val detail_name: String, val detail_count: Int, val detail_cost: Int)
*/