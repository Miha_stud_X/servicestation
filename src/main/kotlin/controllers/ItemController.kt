package controllers

import javafx.collections.FXCollections
import models.*
import tornadofx.Controller
import tornadofx.observable
import tornadofx.rebind
import views.customers.CustomersList

class ItemController : Controller() {
    val itemsC = FXCollections.observableArrayList<CustomerTable>()
    val itemsW = FXCollections.observableArrayList<WorkerTable>()
    val itemsS = FXCollections.observableArrayList<ScladTable>()
    val itemsRW = FXCollections.observableArrayList<RepairWorkTable>()
    val itemsRWC = FXCollections.observableArrayList<RepairWorkTable>()
    val itemsPositin = FXCollections.observableArrayList<Position>()
    val itemsChek = FXCollections.observableArrayList<Chek>()
    val itemsReference = FXCollections.observableArrayList<Reference>()
    val selectedItemC = CustomerModel()
    val selectedItemW = WorkerModel()
    val selectedItemS = ScladModel()
    val selectedItemRW = RepairWorkModel()
    val selectedItemRWC = RepairWorkModel()
    val request = RequestsDB()

    init{
        val resultRequestC = request.getCustomers()
        itemsC.addAll(resultRequestC.observable())
        val resultRequestW = request.getWorkers()
        itemsW.addAll(resultRequestW.observable())
        val resultRequestS = request.getDetails()
        itemsS.addAll(resultRequestS.observable())
        val resultRequestRW = request.getReapairWorks()
        itemsRW.addAll(resultRequestRW.observable())
    }
    fun refreshC(){
        val resultRequestC = request.getCustomers()
        itemsC.clear()
        itemsC.addAll(resultRequestC.observable())
    }
    fun refreshW(){
        val resultRequestW = request.getWorkers()
        itemsW.clear()
        itemsW.addAll(resultRequestW.observable())
    }
    fun refreshS(){
        val resultRequestS = request.getDetails()
        itemsS.clear()
        itemsS.addAll(resultRequestS.observable())
    }
    fun reapairWorksCustomer(n : Int){
        val resultRequestRWC = request.getReapairWorksCustomer(n)
        itemsRWC.clear()
        itemsRWC.addAll(resultRequestRWC.observable())
    }
    fun reapairWorksWorker(n : Int){
        val resultRequestRWC = request.getReapairWorksWorker(n)
        itemsRWC.clear()
        itemsRWC.addAll(resultRequestRWC.observable())
    }
    fun chek(n: Int){
        itemsChek.clear()
        itemsChek.addAll(request.getChek(n).observable())
    }
    fun refernce(){
        itemsReference.clear()
        itemsReference.addAll(request.getRefefence().observable())
    }
}