package controllers

import javafx.beans.property.SimpleStringProperty
import models.RequestsDB
import models.User
import models.UserModel
import models.User_db
import views.LoginView
import views.MainView
import tornadofx.*

class LoginController : Controller() {
    val statusProperty = SimpleStringProperty("")
    var status by statusProperty
    val user: UserModel by inject()

    fun login(username: String, password: String) {
        runLater { status = "" }
        val requestLogin = RequestsDB()
        val propertyItem = requestLogin.getPassword(username, password)
        runLater {
            if (propertyItem>0) {
                user.item = User().apply {
                    name = username
                    level = propertyItem
                }
                find(LoginView::class).replaceWith(MainView::class, sizeToScene = true, centerOnScreen = true)
            } else {
                status = "Проверьте ввод"
            }
        }

    }

    fun logout() {
        user.item = null
        primaryStage.uiComponent<UIComponent>()?.replaceWith(LoginView::class, sizeToScene = true, centerOnScreen = true)
    }

}
