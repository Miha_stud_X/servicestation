import tornadofx.App
import tornadofx.launch
import views.LoginView

class STOApp : App(LoginView::class)

fun main(args: Array<String>) {
    launch<STOApp>(args)
}